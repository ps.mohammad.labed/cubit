part of 'weather_cubit.dart';

@immutable
abstract class WeatherState {}
class Weatherinitial extends WeatherState{}
class Weatherloading extends WeatherState{}
class Weathersuccess extends WeatherState{}
class Weatherfailed extends WeatherState{}