import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:weather_app/model/model.dart';
import 'package:weather_app/services/weather_servecies.dart';

part 'weather_state.dart';

class WeatherCubit extends Cubit<WeatherState> {
  WeatherCubit(this.weatherserv) : super(Weatherinitial());
  WeatherService  weatherserv;
  WeatherModel? model;
  String ?cityname;

  getweatherdata(String cityName)async{
    try{
 emit(Weatherloading());
  model=  await weatherserv.getWeather(cityName: cityName);
  emit(Weathersuccess());

    }
    catch(e){
      emit(Weatherfailed());
    }
   


  }
  
}
