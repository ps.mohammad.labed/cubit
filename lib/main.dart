import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';



import 'package:weather_app/cubit/weather_cubit.dart';

import 'package:weather_app/pages/home_page.dart';

import 'package:weather_app/services/weather_servecies.dart';

void main() {
  runApp(BlocProvider<WeatherCubit>(
     create: (context) {
        return WeatherCubit(WeatherService());
      },
    child: const WeatherApp()));
}

class WeatherApp extends StatelessWidget {
  const WeatherApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
      primarySwatch: Colors.cyan 
      ),
      home:  HomePage(),
    );
  }
}
