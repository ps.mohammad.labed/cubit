import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:weather_app/cubit/weather_cubit.dart';
import 'package:weather_app/pages/home_page.dart';



import '../model/model.dart';


class SearchPage extends StatelessWidget {

  SearchPage({super.key});


  String? cityName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search a City'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: TextField(
            onChanged: (data) {
              cityName = data;
            },
            onSubmitted: (data) async {
              cityName = data;

 BlocProvider.of<WeatherCubit>(context).getweatherdata(cityName!);
              BlocProvider.of<WeatherCubit>(context).cityname = cityName;

              Navigator.pop(context);
            },
            decoration: InputDecoration(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
              label: const Text('search'),
              suffixIcon: GestureDetector(
                  onTap: ()  {
                    BlocProvider.of<WeatherCubit>(context).getweatherdata(cityName!);
                       
               

                    WeatherModel? weather =BlocProvider.of<WeatherCubit>(context).model;
                        

                  
                    BlocProvider.of<WeatherCubit>(context).cityname=cityName;
                    Navigator.pushReplacement(context,MaterialPageRoute(builder: ((context) => HomePage())));
                  },
                  child: const Icon(Icons.search)),
              border:const OutlineInputBorder(),
              hintText: 'Enter a city',
            ),
          ),
        ),
      ),
    );
  }
}
